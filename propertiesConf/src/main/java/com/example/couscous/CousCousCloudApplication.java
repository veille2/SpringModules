package com.example.couscous;



import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import com.example.couscous.model.Ingredient;
import com.example.couscous.model.Type;
import com.example.couscous.repository.IngredientRepository;

@SpringBootApplication
//@EnableWebSecurity
public class CousCousCloudApplication {

	public static void main(String[] args) {
		SpringApplication.run(CousCousCloudApplication.class, args);
	}

	  @Bean
	  public CommandLineRunner dataLoader(IngredientRepository repo) {
	    return new CommandLineRunner() {
	      @Override
	      public void run(String... args) throws Exception {
	        repo.save(new Ingredient("FIN", "Semoule fine", Type.SEMOULE));
	        repo.save(new Ingredient("MOY", "Semoule moyenne", Type.SEMOULE));
	        repo.save(new Ingredient("GROS", "Semoule grosse", Type.SEMOULE));
	        repo.save(new Ingredient("PL", "poulet", Type.VIANDE));
	        repo.save(new Ingredient("BEF", "boeuf", Type.VIANDE));
	        repo.save(new Ingredient("VEA", "veau", Type.VIANDE));
	        repo.save(new Ingredient("OGN", "oignon", Type.LEGUME));
	        repo.save(new Ingredient("CAR", "carrotes", Type.LEGUME));
	        repo.save(new Ingredient("HAR", "harricots verts ", Type.LEGUME));
	        repo.save(new Ingredient("CRG", "courgettes", Type.LEGUME));
	        repo.save(new Ingredient("BCH", "sauce blanche", Type.SAUCE));
	        repo.save(new Ingredient("RGE", "sauce rouge", Type.SAUCE));
	      }
	    };
	  }
	
}
