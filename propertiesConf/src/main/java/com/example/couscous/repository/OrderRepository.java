package com.example.couscous.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.example.couscous.model.Order;
import com.example.couscous.model.User;



public interface OrderRepository 
         extends CrudRepository<Order, Long> {

	Object findByUserOrderByPlacedAtDesc(User user, Pageable pageable);

}
