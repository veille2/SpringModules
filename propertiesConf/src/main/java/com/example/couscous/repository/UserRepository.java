package com.example.couscous.repository;
import org.springframework.data.repository.CrudRepository;

import com.example.couscous.model.User;

public interface UserRepository extends CrudRepository<User, Long> {

  User findByUsername(String username);
  
}
