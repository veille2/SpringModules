package com.example.couscous.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.couscous.model.Ingredient;



public interface IngredientRepository 
         extends CrudRepository<Ingredient, String> {

}
