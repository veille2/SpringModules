package com.example.couscous.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;

@Configuration
@SuppressWarnings("deprecation")
// turns on a variety of beans needed to use Spring Security.
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	// @Autowired
	// DataSource dataSource;
	@Autowired
	private UserDetailsService userDetailsService;
	/**
	 * Defining users in an in-memory user store
	 * 
	 * The in-memory user store is convenient for testing purposes or for very
	 * simple applications, but it doesn’t allow for easy editing of users. If you
	 * need to add, remove, or change a user, you’ll have to make the necessary
	 * changes and then rebuild and redeploy the application.
	 */
	/*
	 * @Override public void configure(AuthenticationManagerBuilder auth) throws
	 * Exception {
	 * auth.inMemoryAuthentication().withUser("user").password("{noop}password").
	 * authorities("ROLE_USER").and().withUser("admin")
	 * .password("{noop}password").authorities("ROLE_ADMIN"); }
	 */

	/**
	 * this config makes some assumptions about your database schema. It expects
	 * that certain tables exist where user data will be kept. More specifically,
	 * the following snippet of code from Spring Security’s internals shows the SQL
	 * queries that will be performed when looking up user details: public static
	 * 
	 * final String DEF_USERS_BY_USERNAME_QUERY = "select username,password,enabled
	 * " + "from users " + "where username = ?";
	 * 
	 * public static final String DEF_AUTHORITIES_BY_USERNAME_QUERY = "select
	 * username,authority " + "from authorities " + "where username = ?";
	 * 
	 * public static final String DEF_GROUP_AUTHORITIES_BY_USERNAME_QUERY = "select
	 * g.id, g.group_name, ga.authority " + "from groups g, group_members gm,
	 * group_authorities ga " + "where gm.username = ? " + "and g.id = ga.group_id "
	 * + "and g.id = gm.group_id";
	 */

	/*
	 * @Override public void configure(AuthenticationManagerBuilder auth) throws
	 * Exception { auth.jdbcAuthentication().dataSource(dataSource); }
	 */

	/**
	 * Customizing user detail queries
	 */
	/*
	 * @Override public void configure(AuthenticationManagerBuilder auth) throws
	 * Exception {
	 * 
	 * auth.jdbcAuthentication()
	 * 
	 * .dataSource(dataSource)
	 * .usersByUsernameQuery("select username, password, enabled from Users " +
	 * "where username=?")
	 * .authoritiesByUsernameQuery("select username, authority from UserAuthorities "
	 * + "where username=?"); }
	 */

	/*
	 * @SuppressWarnings("deprecation")
	 * 
	 * @Override protected void configure(AuthenticationManagerBuilder auth) throws
	 * Exception {
	 * 
	 * auth .jdbcAuthentication() .dataSource(dataSource) .usersByUsernameQuery(
	 * "select username, password, enabled from Users " + "where username=?")
	 * .authoritiesByUsernameQuery(
	 * "select username, authority from UserAuthorities " + "where username=?")
	 * .passwordEncoder(new StandardPasswordEncoder("53cr3t"));
	 * 
	 * }
	 */

	/*************************** LDAP *****************************/
	
	/**
	 * By default, the base queries for both users and groups are empty, indicating
	 * that the search will be done from the root of the LDAP hierarchy. But you can
	 * change that by specifying a query base:
	 */
	/*
	 * @Override protected void configure(AuthenticationManagerBuilder auth) throws
	 * Exception { auth .ldapAuthentication() .userSearchFilter("(uid={0})")
	 * .groupSearchFilter("member={0}"); }
	 */

	/**
	 * The userSearchBase() method provides a base query for finding users.
	 * Likewise, the groupSearchBase() method specifies the base query for finding
	 * groups. Rather than search from the root, this example specifies that users
	 * be searched for where the orga- nizational unit is people. Groups should be
	 * searched for where the organizational unit is groups.
	 */
	/*
	 * @Override protected void configure(AuthenticationManagerBuilder auth) throws
	 * Exception {
	 * auth.ldapAuthentication().userSearchBase("ou=people").userSearchFilter(
	 * "(uid={0})").groupSearchBase("ou=groups") .groupSearchFilter("member={0}"); }
	 */

	/*
	 * By default, the password given in the login form will be compared with the
	 * value of the userPassword attribute in the user’s LDAP entry. If the password
	 * is kept in a different attribute, you can specify the password attribute’s
	 * name with passwordAttribute():
	 * 
	 */
	/*
	 * @Override protected void configure(AuthenticationManagerBuilder auth) throws
	 * Exception {
	 * auth.ldapAuthentication().userSearchBase("ou=people").userSearchFilter(
	 * "(uid={0})").groupSearchBase("ou=groups")
	 * .groupSearchFilter("member={0}").passwordCompare().passwordEncoder(new
	 * BCryptPasswordEncoder()) .passwordAttribute("passcode"); }
	 */

	/*
	 * By default, Spring Security’s LDAP authentication assumes that the LDAP
	 * server is listening on port 33389 on localhost. But if your LDAP server is on
	 * another machine, you can use the contextSource() method to configure the
	 * location:
	 * 
	 */
	/*
	 * @Override protected void configure(AuthenticationManagerBuilder auth) throws
	 * Exception { auth .ldapAuthentication() .userSearchBase("ou=people")
	 * .userSearchFilter("(uid={0})") .groupSearchBase("ou=groups")
	 * .groupSearchFilter("member={0}") .passwordCompare() .passwordEncoder(new
	 * BCryptPasswordEncoder()) .passwordAttribute("passcode"); }
	 */

	/*
	 * @Override protected void configure(AuthenticationManagerBuilder auth) throws
	 * Exception { auth .ldapAuthentication() .userSearchBase("ou=people")
	 * .userSearchFilter("(uid={0})") .groupSearchBase("ou=groups")
	 * .groupSearchFilter("member={0}").contextSource()
	 * .url("ldap://localhost:8389/dc=couscouscloud,dc=com") //
	 * .root("dc=couscouscloud,dc=com"). .and()
	 * //ldif("classpath:test-server.ldif"); .passwordCompare() .passwordEncoder(new
	 * BCryptPasswordEncoder()) .passwordAttribute("passcode");
	 * 
	 * 
	 * 
	 * 
	 * }
	 */

	@SuppressWarnings("deprecation")
	@Bean
	public PasswordEncoder encoder() {
		return new StandardPasswordEncoder("53cr3t");
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {

		auth.userDetailsService(userDetailsService).passwordEncoder(encoder());

	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/design", "/orders").access("hasRole('ROLE_USER')")
				.antMatchers("/", "/**").access("permitAll")
				// end::authorizeRequests[]

				.and().formLogin().loginPage("/login")

				.and().logout().logoutSuccessUrl("/")

				// Make H2-Console non-secured; for debug purposes
				.and().csrf().ignoringAntMatchers("/h2-console/**")

				// Allow pages to be loaded in frames from the same origin; needed for
				// H2-Console
				.and().headers().frameOptions().sameOrigin();
			//	.and().csrf().disable();
	}
}
