/**
 * 
 */
package com.example.couscous.model;

/**
 * @author lydiasaighi
 *
 */
public enum Type {
	SEMOULE, SAUCE,LEGUME, VIANDE

}
