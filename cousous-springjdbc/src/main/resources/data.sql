delete from CousCous_Order_CousCous;
delete from CousCous_Ingredients;
delete from CousCous;
delete from CousCous_Order;
delete from Ingredient;
insert into Ingredient (id, name, type)
                values ('FINE', 'Semoule fine', 'SEMOULE');
insert into Ingredient (id, name, type)
                values ('MOY', 'Semoule moyenne', 'SEMOULE');
 insert into Ingredient (id, name, type)
                values ('GROS', 'Semoule grosse', 'SEMOULE');
insert into Ingredient (id, name, type)
                values ('BF', 'viande de Bouef', 'VIANDE');
insert into Ingredient (id, name, type)
                values ('AGN', 'viande d''Agneau', 'VIANDE');
insert into Ingredient (id, name, type)
                values ('PLET', 'viande de poulet', 'VIANDE');
                
insert into Ingredient (id, name, type)
                values ('HAC', 'Haricots vertes', 'LEGUME');
insert into Ingredient (id, name, type)
                values ('CRG', 'courgettes', 'LEGUME');
insert into Ingredient (id, name, type)
                values ('CAR', 'carrotes', 'LEGUME');
insert into Ingredient (id, name, type)
                values ('OIG', 'oignons', 'LEGUME');
                
insert into Ingredient (id, name, type)
                values ('BL', 'sauce Blanche', 'SAUCE');
insert into Ingredient (id, name, type)
                values ('RGE', 'sauce Rouge', 'SAUCE');