create table if not exists Ingredient (
  id varchar(4) not null,
  name varchar(25) not null,
  type varchar(10) not null
);
create table if not exists CousCous (
  id identity,
  name varchar(50) not null,
  createdAt timestamp not null
);
create table if not exists CousCous_Ingredients (
  couscous bigint not null,
  ingredient varchar(4) not null
);
alter table CousCous_Ingredients
    add foreign key (couscous) references CousCous(id);
alter table CousCous_Ingredients
    add foreign key (ingredient) references Ingredient(id);
    
create table if not exists CousCous_Order (
  id identity,
    deliveryName varchar(50) not null,
    deliveryStreet varchar(50) not null,
    deliveryCity varchar(50) not null,
    deliveryState varchar(2) not null,
    deliveryZip varchar(10) not null,
    ccNumber varchar(16) not null,
    ccExpiration varchar(5) not null,
    ccCVV varchar(3) not null,
    placedAt timestamp not null
);
create table if not exists CousCous_Order_CousCous (
  couscousOrder bigint not null,
  couscous bigint not null
);
alter table CousCous_Order_CousCous
    add foreign key (couscousOrder) references CousCous_Order(id);
alter table CousCous_Order_CousCous
    add foreign key (couscous) references CousCous(id);