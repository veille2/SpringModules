/**
 * 
 */
package com.example.couscous.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.example.couscous.model.CousCous;
import com.example.couscous.model.Ingredient;
import com.example.couscous.model.Order;
import com.example.couscous.model.Type;
import com.example.couscous.repository.CousCousRepository;
import com.example.couscous.repository.IngredientRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * @author lydiasaighi
 *
 */

@Controller
@Slf4j
@RequestMapping("/design")
@SessionAttributes("order")
public class DesignCousCousController {
	private IngredientRepository ingredientRepository;
	private CousCousRepository    cousCousRepository;

	@Autowired
	public DesignCousCousController(IngredientRepository ingredientRepository, CousCousRepository    cousCousRepository) {
		this.ingredientRepository = ingredientRepository;
		this.cousCousRepository   = cousCousRepository;
	}
	
	
	@ModelAttribute(name = "order")
	  public Order order() {
	    return new Order();
	  }
	  @ModelAttribute(name = "design")
	  public CousCous design() {
	    return new CousCous();
	  }

	@GetMapping
	public String showDesignForm(Model model) {
		List<Ingredient> ingredients = new ArrayList<>();
		ingredientRepository.findAll().forEach(ingredients :: add);
		
		Type[] types = Type.values();
		for (Type type : types) {
			model.addAttribute(type.toString().toLowerCase(), filterByType(ingredients, type));
		}
		
		return "design1";
	}

	private List<Ingredient> filterByType(List<Ingredient> ingredients, Type type) {

		return ingredients.stream().filter(x -> x.getType().equals(type)).collect(Collectors.toList());
	}
	
	

	@PostMapping
	public String processDesign(@Valid CousCous design, Errors errors, @ModelAttribute Order order) {
		if (errors.hasErrors()) {
			return "design1";
		}
		
		log.info("Processing couscous: " + design);
		
		CousCous res = cousCousRepository.save(design);
		order.addCousCous(res);
		
		return "redirect:/orders/current";
	}
}
