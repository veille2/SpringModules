/**
 * 
 */
package com.example.couscous.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author lydiasaighi
 *
 */
@Controller
public class HomeController {
	@GetMapping("/")
	public String hello () {
		return "home";
	}

}
