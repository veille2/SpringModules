/**
 * 
 */
package com.example.couscous.repository;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.example.couscous.mapper.MapRowToIngredient;
import com.example.couscous.model.Ingredient;

/**
 * @author lydiasaighi
 *
 */
@Repository
public class JdbcIngredientRepository implements IngredientRepository {
	private JdbcTemplate jdbcTempaate;

	@Autowired
	public JdbcIngredientRepository(JdbcTemplate jdbcTempaate) {
		this.jdbcTempaate = jdbcTempaate;

	}

	@Override
	public Iterable<Ingredient> findAll() {

		return jdbcTempaate.query("select id, name, type from Ingredient ", new MapRowToIngredient());
	}

	@Override
	public Ingredient findOne(String id) {
		return jdbcTempaate.queryForObject("select id, name, type from ingredients where id = ?",
				new MapRowToIngredient(), id);
	}

	@Override
	public Ingredient save(Ingredient ingredient) {
		Object[] parameters = { ingredient.getId(), ingredient.getName(), ingredient.getType().toString() };

		jdbcTempaate.update("insert into ingredient values (?,?,?)", parameters);
		return ingredient;
	}

}
