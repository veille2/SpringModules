/**
 * 
 */
package com.example.couscous.repository;

import com.example.couscous.model.Ingredient;

/**
 * @author lydiasaighi
 *
 */
public interface IngredientRepository {
	
	Iterable<Ingredient> findAll();

	Ingredient findOne(String id);

	Ingredient save(Ingredient ingredient);
}
