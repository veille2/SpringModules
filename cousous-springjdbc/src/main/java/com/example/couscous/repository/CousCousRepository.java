/**
 * 
 */
package com.example.couscous.repository;

import com.example.couscous.model.CousCous;

/**
 * @author lydiasaighi
 *
 */
public interface CousCousRepository {
	
	public CousCous save ( CousCous cousCous);

}
