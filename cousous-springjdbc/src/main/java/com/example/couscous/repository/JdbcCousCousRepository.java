/**
 * 
 */
package com.example.couscous.repository;

import java.sql.Timestamp;
import java.sql.Types;
import java.util.Arrays;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.example.couscous.model.CousCous;
import com.example.couscous.model.Ingredient;

/**
 * @author lydiasaighi
 *
 */
@Repository
public class JdbcCousCousRepository implements CousCousRepository {

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public JdbcCousCousRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.example.couscous.repository.CousCousRepository#save(com.example.couscous.
	 * model.CousCous)
	 */
	@Override
	public CousCous save(CousCous cousCous) {
		long cousCousId = saveCousCousInfo(cousCous);
		cousCous.setId(cousCousId);
		for (Ingredient ing : cousCous.getIngredients()) {
			saveIngredientToCousCous(ing, cousCousId);
		}

		return null;
	}

	private long saveCousCousInfo(CousCous cousCous) {
		cousCous.setCreatedAt(new Date());

		PreparedStatementCreator psc = new PreparedStatementCreatorFactory(
				"insert into CousCous (name, createdAt) values (?, ?)", Types.VARCHAR, Types.TIMESTAMP)
						.newPreparedStatementCreator(
								Arrays.asList(cousCous.getName(), new Timestamp(cousCous.getCreatedAt().getTime())));

		KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(psc, keyHolder);
		return keyHolder.getKey().longValue();
	}

	private void saveIngredientToCousCous(Ingredient ingredient, long cousCousId) {
		jdbcTemplate.update("insert into CousCous_Ingredients (couscous, ingredient) " + "values (?, ?)", cousCousId,
				ingredient.getId());
	}
}
