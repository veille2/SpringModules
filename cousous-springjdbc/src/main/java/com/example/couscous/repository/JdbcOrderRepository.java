/**
 * 
 */
package com.example.couscous.repository;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.example.couscous.model.CousCous;
import com.example.couscous.model.Order;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author lydiasaighi
 *
 */
@Repository
public class JdbcOrderRepository implements OrderRepository {

	private SimpleJdbcInsert orderInserter;
	private SimpleJdbcInsert orderTacoInserter;
	private ObjectMapper objectMapper;

	@Autowired
	public JdbcOrderRepository(JdbcTemplate jdbcTemplate) {
		this.orderInserter = new SimpleJdbcInsert(jdbcTemplate).withTableName("CousCous_Order").usingGeneratedKeyColumns("id");
		this.orderTacoInserter = new SimpleJdbcInsert(jdbcTemplate).withTableName("CousCous_Order_CousCous");
		this.objectMapper = new ObjectMapper();
	}

	@Override
	public Order save(Order order) {
		order.setPlacedAt(new Date());
		long orderId = saveOrderDetails(order);
		order.setId(orderId);
		List<CousCous> allCousCous = order.getAllCousCous();
		for (CousCous cousCous : allCousCous) {
			saveCousCousToOrder(cousCous, orderId);
		}
		return order;
	}

	private long saveOrderDetails(Order order) {
		@SuppressWarnings("unchecked")
		Map<String, Object> values = objectMapper.convertValue(order, Map.class);
		values.put("placedAt", order.getPlacedAt());
		return orderInserter.executeAndReturnKey(values).longValue();

	}

	private void saveCousCousToOrder(CousCous cousCous, long orderId) {
		Map<String, Object> values = new HashMap<>();
		values.put("couscousOrder", orderId);
		values.put("couscous", cousCous.getId());
		orderTacoInserter.execute(values);
	}

}
