/**
 * 
 */
package com.example.couscous.repository;

import com.example.couscous.model.Order;

/**
 * @author lydiasaighi
 *
 */
public interface OrderRepository {
	
	public Order save (Order order);

}
