/**
 * 
 */
package com.example.couscous.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.couscous.model.Ingredient;
import com.example.couscous.model.Type;

/**
 * @author lydiasaighi
 *
 */
public class MapRowToIngredient implements RowMapper<Ingredient> {

	@Override
	public Ingredient mapRow(ResultSet rs, int rowNum) throws SQLException {
		if (rs != null) {
			return new Ingredient(rs.getString("id"), rs.getString("name"), Type.valueOf(rs.getString("type")));
		}
		return null;
	}

}
