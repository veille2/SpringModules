/**
 * 
 */
package com.example.couscous.model;


import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

/**
 * @author lydiasaighi
 *
 */
@Data
public class CousCous {
	@NotNull
	@Size(min = 5, message = "Name must be at least 5 characters long")
	private String name;
	@NotNull
	@Size(min = 4, message = "you must choose at least 4  ingredient")
	private List<Ingredient> ingredients;
	
	   private Long id;
       private Date createdAt;

}
