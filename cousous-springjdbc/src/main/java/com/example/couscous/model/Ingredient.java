/**
 * 
 */
package com.example.couscous.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * @author lydiasaighi
 *
 */
@Data
@RequiredArgsConstructor
public class Ingredient {
	 private final String id;
     private final String name;
     private final Type type;

}
