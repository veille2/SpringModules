/**
 * 
 */
package com.example.couscous.model;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.CreditCardNumber;

import lombok.Data;

/**
 * @author lydiasaighi
 *
 */
@Data
public class Order {
	
    private Long id;
    private Date placedAt;
	@NotBlank(message = "Name is required")
	private String name;
	@NotBlank(message = "street is required")
	private String street;
	@NotBlank(message = "city is required")
	private String city;
	@NotBlank(message = "state is required")
	private String state;
	@NotBlank(message = "zip is required")
	private String zip;
	/**
	 * This annotation declares that the property’s value must be a valid credit card number that passes the Luhn algorithm
	 */
	@CreditCardNumber(message = "Not a valid credit card number")
	private String ccNumber;
	@Pattern(regexp = "^(0[1-9]|1[0-2])([\\/])([1-9][0-9])$", message = "Must be formatted MM/YY")
	private String ccExpiration;
	private String ccCVV;
	
	private List<CousCous> allCousCous = new ArrayList<>();
	  
	  public void addCousCous(CousCous cousCous) {
	    this.allCousCous.add(cousCous);
	  }
	  

}
