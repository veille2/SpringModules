package com.example.couscous.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.couscous.model.CousCous;


public interface CousCousRepository 
         extends CrudRepository<CousCous, Long> {

}
