package com.example.couscous.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.couscous.model.Order;



public interface OrderRepository 
         extends CrudRepository<Order, Long> {

}
