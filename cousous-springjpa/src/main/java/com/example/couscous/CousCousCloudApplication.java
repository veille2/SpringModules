package com.example.couscous;



import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CousCousCloudApplication {

	public static void main(String[] args) {
		SpringApplication.run(CousCousCloudApplication.class, args);
	}

	
}
