/**
 * 
 */
package com.example.couscous.model;


import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.PrePersist;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;


/**
 * @author lydiasaighi
 *
 */
@Data
@Entity
public class CousCous {
	@Id
	@GeneratedValue(strategy =GenerationType.AUTO)
	private Long id;
    private Date createdAt;
	@NotNull
	@Size(min = 5, message = "Name must be at least 5 characters long")
	private String name;
	@NotNull
	@Size(min = 4, message = "you must choose at least 4  ingredient")
	@ManyToMany(targetEntity = Ingredient.class)
	private List<Ingredient> ingredients;
	
	@PrePersist
	  void createdAt() {
	    this.createdAt = new Date();
	  }
	   

}
